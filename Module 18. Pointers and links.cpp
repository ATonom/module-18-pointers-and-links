﻿// Module 18. Pointers and links.cpp

#include <iostream>
#include <string>

template<typename T>
class Stack {
public:

    Stack(int newSize = 4) {
        count = 0;
        size = newSize;
        data = new T[newSize];
    }

    void Push(T item) {
        if (count < size) data[count++] = item;

        else {
            if (reSize(size * 2)) data[count++] = item;
        }
    }

    T Pop() {
        if (count == 0) return false;
        return data[--count];
    }

    bool reSize(int newSize) {
        T* tmp = data;
        int tmpSize = size;

        try {
            data = new T[newSize];

            if (newSize < tmpSize) {
                tmpSize = newSize;
                if (count > newSize) count = newSize;
            }

            for (int i = 0; i < tmpSize; i++) data[i] = tmp[i];

            size = newSize;
            return true;
        }
        catch (std::bad_alloc e) {
            std::cout << e.what() << std::endl;

            data = tmp;
            return false;
        }

        delete[] tmp;
    }

    int GetSize() {
        return size;
    }

private:
    int count;
    int size;
    T* data;
};


int main()
{
    Stack<int> s1;

    for ( int i = 0; i < 11; i++ ) s1.Push(i + 1);
    s1.reSize(8);
    for (int i = 0; i < 11; i++) std::cout << s1.Pop() << std::endl;

    Stack<char> s2;

    for (int i = 64; i < 74; i++) s2.Push(i + 1);
    for (int i = 0; i < 10; i++) std::cout << s2.Pop() << std::endl;

}

/*
1. Создайте класс Stack, который будет реализовывать принцип работы структуры данных «стек».
Размер стека не должен быть ограничен (используйте динамический массив).
2. Стек можно реализовать для любого типа данных на выбор (int, float, double, string).
Должны быть как минимум методы pop(), чтобы достать из стека верхний элемент, и push(),
чтобы добавить новый элемент.
3. Протестируйте свою структуру.
При желании можно выполнить усложнённую версию задания и реализовать стек при помощи шаблонов
(необходимо самостоятельно ознакомиться с материалом).
*/